import React, { Component } from 'react';
import './App.css';
import Dashboard from './modules/shared/components/dashboard';

class App extends Component {

  render() {
    return (
      <div>
        <Dashboard>
        </Dashboard>
      </div>
    );
  }
}

export default App;
