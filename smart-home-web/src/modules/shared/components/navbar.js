// React
import React from 'react';

// Components
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import TypoGraphy from '@material-ui/core/Typography';
import Button from "@material-ui/core/Button";
import Link from '@material-ui/core/Link';

// Icons
import { Dashboard, Home, MeetingRoom, Devices } from '@material-ui/icons';

// Routing
import { Link as RouterLink } from 'react-router-dom';

class NavBar extends React.Component {
    render() {
        return (
            <List component="nav">
                
                <ListItem component="div">
                    <ListItemText inset>
                        <TypoGraphy color="inherit" variant="title">
                        <Button color="inherit">
                            <Link color="inherit" component={RouterLink} to="/dashboard">Dashboard </Link>
                        </Button> <Dashboard />
                        </TypoGraphy>
                    </ListItemText>
    
                    <ListItemText inset>
                        <TypoGraphy color="inherit" variant="title">
                        <Button color="inherit">
                            <Link color="inherit" component={RouterLink} to="/workspace">Workspaces </Link>
                        </Button> <Home />
                        </TypoGraphy>
                    </ListItemText>
    
                    <ListItemText inset>
                        <TypoGraphy color="inherit" variant="title">
                        <Button color="inherit">
                            Rooms
                        </Button><MeetingRoom />
                        </TypoGraphy>
                    </ListItemText>
     
                    <ListItemText inset>
                        <TypoGraphy color="inherit" variant="title">
                        <Button color="inherit">
                            Devices
                        </Button> <Devices />
                        </TypoGraphy>
                    </ListItemText>
                </ListItem >
    
            </List>
        )
    }
}


export default NavBar;