// React
import React from 'react';

// Components
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function AlertDialogSlide(props) {
  const [open, setOpen] = React.useState(false);

  function handleClose() {
    setOpen(false);
    props.onClose({
        close: true
    });
  }

  React.useEffect(() => {
    if (props.show) {
        setOpen(true);
    }
    else {
        setOpen(false);
    }
  }, props.show);

  return (
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">{"Validation error"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
                You have to provide all the field data
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button fullWidth onClick={handleClose} color="secondary">
            CLOSE
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default AlertDialogSlide;