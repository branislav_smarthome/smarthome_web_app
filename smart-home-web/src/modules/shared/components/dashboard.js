// React
import React from "react";

// Components
import Card from "@material-ui/core/Card";
import { Grid, Typography } from "@material-ui/core";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";

// Images
import WorkspaceImage from '../../../resource/workspace.png';
import RoomImage from '../../../resource/room.jpg';
import DeviceImage from '../../../resource/device.png';

// Routing
import { Link } from 'react-router-dom';

function Dashboard (props) {

    const dashboardItems = [
        { 
            title: 'Workspace',
            image: WorkspaceImage,
            excerpt: 'Here you can overview your workspaces and create new ones',
            url: '/workspace'
        },
        {
            title: 'Room',
            image: RoomImage,
            excerpt: 'Here you can overview your rooms and create new ones',
            url: '/room'
        },
        {
            title: 'Device',
            image: DeviceImage,
            excerpt: 'Here you can overview your devices and create new ones',
            url: '/device'
        }
    ]

  return (
    <div style={{ marginTop: 20, padding: 50 }}>
      <Grid container direction="row" alignItems="center" spacing={3} justify="center">
        {dashboardItems.map(item => (
          <Grid item key={item.title} xs={3} >
            <Card>
              <CardActionArea>
                <Link to={item.url} color="inherit">
                  <CardMedia style={{height: 0, paddingTop: '56.25%' }}
                            image={item.image}/>
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                      {item.title}
                    </Typography>
                    <Typography component="p">{item.excerpt}</Typography>
                  </CardContent>      
                </Link>
              </CardActionArea>
              <CardActions>
                <Button fullWidth={true} size="small" color="primary">
                  <Link to={item.url} color="inherit">GO TO {item.title}</Link>
                </Button>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </div>
  );
}

export default Dashboard;