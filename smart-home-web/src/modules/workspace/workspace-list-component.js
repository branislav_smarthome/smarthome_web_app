// React
import React, { Component } from 'react';

// Components
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import Button from "@material-ui/core/Button";
import Link from '@material-ui/core/Link';
import WorkspaceRow from './workspace-row-component';

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

// Data
import axios from 'axios';

// Routing
import { Link as RouterLink } from 'react-router-dom';

class WorkspaceListComponent extends Component {

  state = {
    workspaces: []
  }

  componentDidMount() { 
    axios.get('http://localhost:8080/api/workspace')
    .then(response => {
      this.setState({ workspaces: response.data })
    })
    .catch(function (error) {
      console.log(error);
    })
  }

  onDelete = (props) => {
      axios.get('http://localhost:8080/api/workspace')
      .then(response => {
        this.setState({ workspaces: response.data })
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    return (
      <div class="container" style={{ marginTop: '50px' }}>
        <div class="row">
          <div class=".col-md-6 .offset-md-3">
            <Button color="primary">
                <Link color="inherit" component={RouterLink} to="/workspace/add">Create workspace </Link>
            </Button>
          </div>
        </div>
        <div>
          <List style={{ backgroundColor: '#e6e6ff' }} subheader={<ListSubheader  style={{ backgroundColor: '#ccccff' }}>Workspace list header</ListSubheader>} alignItems="center" component="nav" xs={4}>
            { this.state.workspaces.map(workspace => (
              <WorkspaceRow workspace={workspace} onDelete={this.onDelete} />
              ))}  
          </List>
        </div>
    </div>
    ); 
  }
}

export default WorkspaceListComponent;