// React
import React, { Component } from 'react';

// Components
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import AlertDialogSlide from '../../shared/components/alert-dialog';

// Data
import axios from 'axios';

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

export default class RoomAddFormComponent extends Component {

    constructor(props) {
        super(props);

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeFloor = this.onChangeFloor.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.goBack = this.goBack.bind(this);

        this.state = {
            room_name: '',
            room_floor: 0,
            error: false
        }
    }


    onChangeName(e) {
        this.setState({
            room_name: e.target.value
        });
    }
    onChangeFloor(e) {
        this.setState({
            room_floor: e.target.value
        })  
    }

    goBack(){
        this.props.history.goBack();
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.state.room_name === "" || this.state.room_floor === "") {
            this.setState({
                error: true
            });
            return;
        }

        const obj = {
          name: this.state.room_name,
          floor: this.state.room_floor,
        };
        axios.post('http://localhost:8080/api/workspace/' + this.props.match.params.uid + '/room', obj)
            .then(res => {
                this.goBack();
            });
        
        this.setState({
            room_name: '',
            room_floor: 0
        });
      }

    handleClose = (props) => {
        this.setState({ error: false })
    }

    render() {
        return(
            <div class="container">
                <form style={{ marginTop: '50px' }}>
                    { this.state.error ? <AlertDialogSlide show={this.state.error} onClose={this.handleClose} > </AlertDialogSlide> : null }
                    <div class="row">
                    <TextField id="standard-full-width"
                                label="Name"
                                placeholder="Enter room name"
                                fullWidth
                                margin="normal"
                                required
                                error={this.state.room_name === ""}
                                helperText={this.state.room_name === "" ? 'Empty field!' : ' '}
                                InputLabelProps={{ shrink: true }}
                                value={this.state.room_name}
                                onChange={this.onChangeName}/>
                    </div>
                    <div class="row">
                    <TextField id="outlined-number"
                                label="Number"
                                placeholder="Enter room floor"
                                fullWidth
                                margin="normal"
                                required
                                type="number"
                                variant="outlined"
                                error={this.state.room_floor === ""}
                                helperText={this.state.room_floor === "" ? 'Empty field!' : ' '}
                                InputLabelProps={{ shrink: true }}
                                value={this.state.room_floor}
                                onChange={this.onChangeFloor}/>
                    </div>
                    <div class="row">
                        <div class="col">
                            <Button onClick={this.onSubmit} fullWidth color="primary">
                                Create room
                            </Button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
