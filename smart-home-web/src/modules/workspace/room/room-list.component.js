// React
import React, { Component } from 'react';

// Components
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import Button from "@material-ui/core/Button";
import Link from '@material-ui/core/Link';
import RoomRowComponent from './room-row-component';

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

// Data
import axios from 'axios';

// Routing
import { Link as RouterLink } from 'react-router-dom';

class RoomListComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            rooms: []
        }
    }
    
    componentDidMount() {
        console.log(this.props);
        axios.get('http://localhost:8080/api/workspace/' + this.props.workspaceUid + '/rooms')
        .then(response => {
          this.setState({ rooms: response.data })
        })
        .catch(function (error) {
          console.log(error);
        })
    }

    onDelete = (props) => {
        this.setState({rooms: this.state.rooms.filter(x => x.uid != props.roomUid)});
    }

    render() {
        return (
          <div class="container" style={{ marginTop: '50px' }}>
            <div class="row">
              <div class=".col-md-6 .offset-md-3">
                <Button color="primary">
                    <Link color="inherit" component={RouterLink} to={this.props.workspaceUid + '/room/add'}>Create room </Link>
                </Button>
              </div>
            </div>
            <div>
              <List style={{ backgroundColor: '#e6e6ff' }} subheader={<ListSubheader  style={{ backgroundColor: '#ccccff' }}>Room list header</ListSubheader>} alignItems="center" component="nav" xs={4}>
                { this.state.rooms.map(room => (
                  <RoomRowComponent room={room} workspaceUid={this.props.workspaceUid} onDelete={this.onDelete} />
                  ))}  
              </List>
            </div>
        </div>
        ); 
      }
}

export default RoomListComponent;