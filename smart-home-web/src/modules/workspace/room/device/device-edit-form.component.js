// React
import React, { Component } from 'react';

// Components
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import AlertDialogSlide from '../../../shared/components/alert-dialog';
import SimpleSelect from './device-type.component';
import DeviceAttributeListComponent from './device-attribute/device-attribute-list.component';

// Data
import axios from 'axios';

export default class DeviceEditFormComponent extends Component {

    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeReferenceId = this.onChangeReferenceId.bind(this);
        this.onChangeLocation = this.onChangeLocation.bind(this);
        this.onChangeDeviceType = this.onChangeDeviceType.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.goBack = this.goBack.bind(this);

        this.state = {
            workspace_uid: '',
            room_uid: '',
            device_uid: '',
            device_name: '',
            device_referenceId: '',
            device_location: '',
            device_type: '',
            error: false
        }
    }

    onChangeName(e) {
        this.setState({
            device_name: e.target.value
        });
    }

    onChangeReferenceId(e) {
        this.setState({
            device_referenceId: e.target.value
        })  
    }

    onChangeLocation(e) {
        this.setState({
            device_location: e.target.value
        });
    }

    onChangeDeviceType(e) {
        this.setState({
            device_type : e.value
        });
    }

    goBack(){
        this.props.history.goBack();
    }

    componentDidMount() {
        axios.get('http://localhost:8080/api/workspace/'+ this.props.match.params.uid + '/room/' + this.props.match.params.roomUid + '/device/' + this.props.match.params.deviceUid)
            .then(response => {
                this.setState({ 
                    workspace_uid: this.props.match.params.uid,
                    room_uid: this.props.match.params.roomUid,
                    device_uid: this.props.match.params.deviceUid,
                    device_name: response.data.name, 
                    device_referenceId: response.data.referenceId,
                    device_location: response.data.location,
                    device_type: response.data.deviceTypeId
                });
            })
            .catch(function (error) {
                console.log(error);
             });
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.state.device_name === "" || this.state.device_referenceId === ""
                || this.state.device_location === "" || this.state.device_type === "") {
            this.setState({
                error: true
            });
            return;
        }

        const obj = {
            name: this.state.device_name,
            referenceId: this.state.device_referenceId,
            location: this.state.device_location,
            deviceType: this.state.device_type
        };
        axios.patch('http://localhost:8080/api/workspace/' + this.props.match.params.uid + '/room/' + this.state.room_uid + '/device/' + this.state.device_uid, obj)
            .then(res => {
                this.goBack();
            });
      }

    handleClose = (props) => {
        this.setState({ error: false })
    }

    render() {
        return(
            <div class="container">
                <form style={{ marginTop: '50px' }}>
                    { this.state.error ? <AlertDialogSlide show={this.state.error} onClose={this.handleClose} > </AlertDialogSlide> : null }
                    <div class="row">
                        <TextField id="standard-full-width"
                                    label="Name"
                                    placeholder="Enter device name"
                                    fullWidth
                                    margin="normal"
                                    required
                                    error={this.state.device_name === ""}
                                    helperText={this.state.device_name === "" ? 'Empty field!' : ' '}
                                    InputLabelProps={{ shrink: true }}
                                    value={this.state.device_name}
                                    onChange={this.onChangeName}/>
                    </div>
                    <div class="row">
                        <TextField id="outlined-number"
                                    label="Number"
                                    placeholder="Enter device reference id"
                                    fullWidth
                                    margin="normal"
                                    required
                                    error={this.state.device_referenceId === ""}
                                    helperText={this.state.device_referenceId === "" ? 'Empty field!' : ' '}
                                    InputLabelProps={{ shrink: true }}
                                    value={this.state.device_referenceId}
                                    onChange={this.onChangeReferenceId}/>
                    </div>
                    <div class="row">
                        <TextField id="outlined-number"
                                    label="Location"
                                    placeholder="Enter device location"
                                    fullWidth
                                    margin="normal"
                                    required
                                    error={this.state.device_location === ""}
                                    helperText={this.state.device_location === "" ? 'Empty field!' : ' '}
                                    InputLabelProps={{ shrink: true }}
                                    value={this.state.device_location}
                                    onChange={this.onChangeLocation}/>
                    </div>
                    <div class="row">
                        <SimpleSelect fullWidth value={this.state.device_type}
                                      onChange={this.onChangeDeviceType}>
                        </SimpleSelect>
                    </div>
                    <div class="row">
                        <div class="col">
                            <Button onClick={this.goBack} fullWidth>
                                GO BACK
                            </Button>
                        </div>
                        <div class="col">
                            <Button onClick={this.onSubmit} fullWidth color="primary">
                                Update
                            </Button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    { this.state.workspace_uid && this.state.room_uid && this.state.device_uid ? 
                        <DeviceAttributeListComponent workspaceUid={this.state.workspace_uid} 
                                             roomUid={this.state.room_uid}
                                             deviceUid={this.state.device_uid}></DeviceAttributeListComponent> : null }
                </div> 
            </div>
        );
    }
}
