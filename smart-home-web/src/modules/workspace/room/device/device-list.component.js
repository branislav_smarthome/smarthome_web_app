// React
import React, { Component } from 'react';

// Components
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import Button from "@material-ui/core/Button";
import Link from '@material-ui/core/Link';
import DeviceRowComponent from './device-row.component';

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

// Data
import axios from 'axios';

// Routing
import { Link as RouterLink } from 'react-router-dom';

class DeviceListComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            devices: []
        }
    }
    
    componentDidMount() {
        console.log(this.props);
        axios.get('http://localhost:8080/api/workspace/' + this.props.workspaceUid + '/room/' + this.props.roomUid + '/devices')
        .then(response => {
          this.setState({ devices: response.data })
        })
        .catch(function (error) {
          console.log(error);
        })
    }

    onDelete = (props) => {
        this.setState({ devices: this.state.devices.filter(x => x.uid != props.deviceUid)});
    }

    render() {
        return (
          <div class="container" style={{ marginTop: '50px' }}>
            <div class="row">
              <div class=".col-md-6 .offset-md-3">
                <Button color="primary">
                    <Link color="inherit" component={RouterLink} to={this.props.roomUid + '/device/add'}>Create device </Link>
                </Button>
              </div>
            </div>
            <div>
              <List style={{ backgroundColor: '#e6e6ff' }} subheader={<ListSubheader  style={{ backgroundColor: '#ccccff' }}>Device list header</ListSubheader>} alignItems="center" component="nav" xs={4}>
                { this.state.devices.map(device => (
                  <DeviceRowComponent device={device} workspaceUid={this.props.workspaceUid} roomUid={this.props.roomUid} onDelete={this.onDelete} />
                  ))}  
              </List>
            </div>
        </div>
        ); 
      }
}

export default DeviceListComponent;