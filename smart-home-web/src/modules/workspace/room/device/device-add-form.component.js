// React
import React, { Component } from 'react';

// Components
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import AlertDialogSlide from '../../../shared/components/alert-dialog';
import SimpleSelect from './device-type.component';

// Data
import axios from 'axios';

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

export default class DeviceAddFormComponent extends Component {

    constructor(props) {
        super(props);

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeReferenceId = this.onChangeReferenceId.bind(this);
        this.onChangeLocation = this.onChangeLocation.bind(this);
        this.onChangeDeviceType = this.onChangeDeviceType.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.goBack = this.goBack.bind(this);

        this.state = {
            device_name: '',
            device_referenceId: '',
            device_location: '',
            device_type: '',
            error: false
        }
    }

    onChangeName(e) {
        this.setState({
            device_name: e.target.value
        });
    }

    onChangeReferenceId(e) {
        this.setState({
            device_referenceId: e.target.value
        })  
    }

    onChangeLocation(e) {
        this.setState({
            device_location: e.target.value
        });
    }

    onChangeDeviceType(e) {
        this.setState({
            device_type : e.value
        });
    }

    goBack(){
        this.props.history.goBack();
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.state.device_name === "" || this.state.device_referenceId === ""
                || this.state.device_location === "" || this.state.device_type === "") {
            this.setState({
                error: true
            });
            return;
        }

        const obj = {
          name: this.state.device_name,
          referenceId: this.state.device_referenceId,
          location: this.state.device_location,
          deviceType: this.state.device_type
        };

        axios.post('http://localhost:8080/api/workspace/' + this.props.match.params.uid + '/room/' + this.props.match.params.roomUid + '/device', obj)
            .then(res => {
                this.goBack();
            });
        
        this.setState({
            device_name: '',
            device_referenceId: '',
            device_location: '',
            device_type: 1
        });
      }

    handleClose = (props) => {
        this.setState({ error: false })
    }

    render() {
        return(
            <div class="container">
                <form style={{ marginTop: '50px' }}>
                    { this.state.error ? <AlertDialogSlide show={this.state.error} onClose={this.handleClose} > </AlertDialogSlide> : null }
                    <div class="row">
                        <TextField id="standard-full-width"
                                    label="Name"
                                    placeholder="Enter device name"
                                    fullWidth
                                    margin="normal"
                                    required
                                    error={this.state.device_name === ""}
                                    helperText={this.state.device_name === "" ? 'Empty field!' : ' '}
                                    InputLabelProps={{ shrink: true }}
                                    value={this.state.device_name}
                                    onChange={this.onChangeName}/>
                    </div>
                    <div class="row">
                        <TextField id="outlined-number"
                                    label="Number"
                                    placeholder="Enter device reference id"
                                    fullWidth
                                    margin="normal"
                                    required
                                    error={this.state.device_referenceId === ""}
                                    helperText={this.state.device_referenceId === "" ? 'Empty field!' : ' '}
                                    InputLabelProps={{ shrink: true }}
                                    value={this.state.device_referenceId}
                                    onChange={this.onChangeReferenceId}/>
                    </div>
                    <div class="row">
                        <TextField id="outlined-number"
                                    label="Location"
                                    placeholder="Enter device location"
                                    fullWidth
                                    margin="normal"
                                    required
                                    error={this.state.device_location === ""}
                                    helperText={this.state.device_location === "" ? 'Empty field!' : ' '}
                                    InputLabelProps={{ shrink: true }}
                                    value={this.state.device_location}
                                    onChange={this.onChangeLocation}/>
                    </div>
                    <div class="row">
                        <SimpleSelect value={this.state.device_type}
                                      onChange={this.onChangeDeviceType}>
                        </SimpleSelect>
                    </div>
                    <div class="row">
                        <div class="col">
                            <Button onClick={this.onSubmit} fullWidth color="primary">
                                Create device
                            </Button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
