import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Dialog from "@material-ui/core/Dialog";
import Input from '@material-ui/core/Input';

// Data
import axios from 'axios';

const styles = theme => ({
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  }
});

class SimpleSelect extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
            name: 'test',
            labelWidth: 0,
            deviceTypes: []
        };
    }


  componentDidMount() {
    this.setState({
      labelWidth: ReactDOM.findDOMNode(this.InputLabelRef.current).offsetWidth
    });

    axios.get('http://localhost:8080/api/workspace/device-types')
        .then(response => {
          this.setState({ deviceTypes: response.data });
          console.log(response.data);
        })
        .catch(function (error) {
          console.log(error);
        })

  }

  handleChange = event => {
    this.props.onChange({
      value: event.target.value 
    })
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { classes } = this.props;

    this.InputLabelRef = React.createRef();
    return (
      <form className={classes.root} autoComplete="off">
        <FormControl variant="outlined" className={classes.formControl} error={this.props.value === '' ? (this.state.id === '') : (this.state.id === '' && this.props.value === '') }>
          <InputLabel ref={this.InputLabelRef} htmlFor="outlined-device-type">
            Device type
          </InputLabel>
          <Select
            value={this.state.id === '' ? this.props.value : this.state.id }
            onChange={this.handleChange}
            input={<Input name="id" id="outlined-device-type" />}>
                <MenuItem value="">
                    <em>None</em>
                </MenuItem>
              { this.state.deviceTypes.map(deviceType => (
                <MenuItem value={deviceType.id}>{deviceType.name}</MenuItem>
                ))}
          </Select>
        </FormControl>
      </form>
    );
  }
}

SimpleSelect.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SimpleSelect);
