// React
import React, { Component } from 'react';

// Components
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import AlertDialogSlide from '../../../../shared/components/alert-dialog';

// Data
import axios from 'axios';

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

export default class DeviceAttributeAddFormComponent extends Component {

    constructor(props) {
        super(props);

        this.onChangeDeviceAttributeKey = this.onChangeDeviceAttributeKey.bind(this);
        this.onChangeDeviceAttributeValue = this.onChangeDeviceAttributeValue.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.goBack = this.goBack.bind(this);

        this.state = {
            device_attribute_key: '',
            device_attribute_value: '',
            error: false
        }
    }

    onChangeDeviceAttributeKey(e) {
        this.setState({
            device_attribute_key: e.target.value
        });
    }

    onChangeDeviceAttributeValue(e) {
        this.setState({
            device_attribute_value: e.target.value
        })  
    }

    goBack(){
        this.props.history.goBack();
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.state.device_attribute_key === "" || this.state.device_attribute_value === "") {
            this.setState({
                error: true
            });
            return;
        }

        const obj = {
          key: this.state.device_attribute_key,
          value: this.state.device_attribute_value
        };

        axios.post('http://localhost:8080/api/workspace/' + this.props.match.params.uid + '/room/' + this.props.match.params.roomUid + '/device/' 
                        + this.props.match.params.deviceUid + '/attribute', obj)
            .then(res => {
                this.goBack();
            });
        
        this.setState({
            device_attribute_key: '',
            device_attribute_value: ''
        });
      }

    handleClose = (props) => {
        this.setState({ error: false })
    }

    render() {
        return(
            <div class="container">
                <form style={{ marginTop: '50px' }}>
                    { this.state.error ? <AlertDialogSlide show={this.state.error} onClose={this.handleClose} > </AlertDialogSlide> : null }
                    <div class="row">
                        <TextField id="standard-full-width"
                                    label="Key"
                                    placeholder="Enter device attribute key"
                                    fullWidth
                                    margin="normal"
                                    required
                                    error={this.state.device_attribute_key === ""}
                                    helperText={this.state.device_attribute_key === "" ? 'Empty field!' : ' '}
                                    InputLabelProps={{ shrink: true }}
                                    value={this.state.device_attribute_key}
                                    onChange={this.onChangeDeviceAttributeKey}/>
                    </div>
                    <div class="row">
                        <TextField id="outlined-number"
                                    label="Value"
                                    placeholder="Enter device attribute value"
                                    fullWidth
                                    margin="normal"
                                    required
                                    error={this.state.device_attribute_value === ""}
                                    helperText={this.state.device_attribute_value === "" ? 'Empty field!' : ' '}
                                    InputLabelProps={{ shrink: true }}
                                    value={this.state.device_attribute_value}
                                    onChange={this.onChangeDeviceAttributeValue}/>
                    </div>
                    <div class="row">
                        <div class="col">
                            <Button onClick={this.onSubmit} fullWidth color="primary">
                                Create device attribute
                            </Button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
