// React
import React, { Component } from 'react';

// Components
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import Button from "@material-ui/core/Button";
import Link from '@material-ui/core/Link';
import DeviceAttributeRowComponent from './device-attribute-row.component';

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

// Data
import axios from 'axios';

// Routing
import { Link as RouterLink } from 'react-router-dom';

class DeviceAttributeListComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            deviceAttributes: []
        }
    }
    
    componentDidMount() {
        console.log(this.props);
        axios.get('http://localhost:8080/api/workspace/' + this.props.workspaceUid + '/room/' + this.props.roomUid + '/device/' + this.props.deviceUid + '/attributes')
        .then(response => {
          this.setState({ deviceAttributes: response.data })
        })
        .catch(function (error) {
          console.log(error);
        })
    }

    onDelete = (props) => {
        this.setState({ deviceAttributes: this.state.deviceAttributes.filter(x => x.uid != props.deviceAttributeUid)});
    }

    render() {
        return (
          <div class="container" style={{ marginTop: '50px' }}>
            <div class="row">
              <div class=".col-md-6 .offset-md-3">
                <Button color="primary">
                    <Link color="inherit" component={RouterLink} to={this.props.deviceUid + '/attribute/add'}>Create device attribute </Link>
                </Button>
              </div>
            </div>
            <div>
              <List style={{ backgroundColor: '#e6e6ff' }} subheader={<ListSubheader  style={{ backgroundColor: '#ccccff' }}>Device attribute list header</ListSubheader>} alignItems="center" component="nav" xs={4}>
                { this.state.deviceAttributes.map(deviceAttribute => (
                  <DeviceAttributeRowComponent workspaceUid={this.props.workspaceUid} 
                                               roomUid={this.props.roomUid} 
                                               deviceUid={this.props.deviceUid}
                                               deviceAttribute={deviceAttribute} 
                                               onDelete={this.onDelete} />
                  ))}  
              </List>
            </div>
        </div>
        ); 
      }
}

export default DeviceAttributeListComponent;