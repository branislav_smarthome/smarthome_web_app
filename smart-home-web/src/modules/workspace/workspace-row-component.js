// React
import React, { Component } from 'react';

// Components
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import Button from "@material-ui/core/Button";

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

// Routing
import { Link } from 'react-router-dom';

// Data
import axios from 'axios';

class WorkspaceRow extends Component {

    constructor(props) {
        super(props);
        this.delete = this.delete.bind(this);
    }

    delete() {
        axios.delete('http://localhost:8080/api/workspace/' + this.props.workspace.uid)
            .then(this.props.onDelete({
                deleted: true
            }))
            .catch(err => console.log(err))
    }

    render() {
        return (
            <ListItem button>
                <div class="col-6">
                    <ListItemText primary={this.props.workspace.name }
                                secondary={
                                    <React.Fragment>
                                    <Typography
                                        component="span"
                                        variant="body2"
                                        style={{display: 'inline'}}
                                        color="textPrimary"
                                    >
                                    { this.props.workspace.description }
                                    </Typography> 
                                    </React.Fragment>
                                } />
                </div>
                <div class="col-6">
                    <div class="row justify-content-end">
                        <div class=".col-md-3 .offset-md-3">
                            <Button color="primary">
                                <Link to={"/workspace/edit/" + this.props.workspace.uid} color="inherit">Edit</Link>
                            </Button>
                        </div>
                        <div class=".col-md-3 .offset-md-3">
                            <Button color="secondary" onClick={this.delete}>Remove</Button>
                        </div>
                    </div>
                </div>
            </ListItem>            
        );
      }
}

export default WorkspaceRow;