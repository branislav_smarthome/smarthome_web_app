// React
import React, { Component } from 'react';

// Components
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import AlertDialogSlide from '../shared/components/alert-dialog';

// Data
import axios from 'axios';

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

export default class WorkspaceAddFormComponent extends Component {

    constructor(props) {
        super(props);

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.goBack = this.goBack.bind(this);

        this.state = {
            workspace_name: '',
            workspace_description: '',
            error: false
        }
    }

    onChangeName(e) {
        this.setState({
            workspace_name: e.target.value
        });
    }
    onChangeDescription(e) {
        this.setState({
            workspace_description: e.target.value
        })  
    }

    goBack(){
        this.props.history.goBack();
    }

    onSubmit(e) {
        e.preventDefault();

        if (this.state.workspace_name === "" || this.state.workspace_description === "") {
            this.setState({
                error: true
            });
            return;
        }

        const obj = {
          name: this.state.workspace_name,
          description: this.state.workspace_description,
        };
        axios.post('http://localhost:8080/api/workspace', obj)
            .then(res => {
                this.goBack();
            });
        
        this.setState({
            workspace_name: '',
            workspace_description: ''
        });
      }

    handleClose = (props) => {
        this.setState({ error: false })
    }

      render() {
        return(
            <div class="container">
                <form style={{ marginTop: '50px' }}>
                    { this.state.error ? <AlertDialogSlide show={this.state.error} onClose={this.handleClose} > </AlertDialogSlide> : null }
                    <div class="row">
                    <TextField id="standard-full-width"
                                label="Name"
                                style={{ margin: 8 }}
                                placeholder="Enter workspace name"
                                fullWidth
                                margin="normal"
                                required
                                error={this.state.workspace_name === ""}
                                helperText={this.state.workspace_name === "" ? 'Empty field!' : ' '}
                                InputLabelProps={{ shrink: true }}
                                value={this.state.workspace_name}
                                onChange={this.onChangeName}/>
                    </div>
                    <div class="row">
                    <TextField id="standard-full-width"
                                label="Description"
                                style={{ margin: 8 }}
                                placeholder="Enter workspace description"
                                fullWidth
                                margin="normal"
                                required
                                error={this.state.workspace_description === ""}
                                helperText={this.state.workspace_description === "" ? 'Empty field!' : ' '}
                                InputLabelProps={{ shrink: true }}
                                value={this.state.workspace_description}
                                onChange={this.onChangeDescription}/>
                    </div>
                    <div class="row">
                        <div class="col">
                            <Button onClick={this.onSubmit} fullWidth color="primary">
                                Create workspace
                            </Button>
                        </div>
                    </div>
                </form>
            </div>
        );
      }
}
