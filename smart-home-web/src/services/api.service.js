// React
import React from 'react';

// Data
import axios from 'axios';

class ApiService extends React.Component {
    
    getWorkspaceAsync = () => {
        axios.get('http://localhost:8080/api/workspace')
        .then(response => {
          return response.data;
        })
    }
}

export default ApiService;