// React
import React from 'react';
import ReactDOM from 'react-dom';

// Components
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import TypoGraphy from '@material-ui/core/Typography';
import NavBar from './modules/shared/components/navbar';
import App from './App';
import WorkspaceListComponent from './modules/workspace/workspace-list-component';
import WorkspaceAddFormComponent from './modules/workspace/workspace-add-form-component';
import WorkspaceEditFormComponent from './modules/workspace/workspace-edit-form.component';
import RoomAddFormComponent from './modules/workspace/room/room-add-form-component';
import RoomEditFormComponent from './modules/workspace/room/room-edit-form-component';
import DeviceAddFormComponent from './modules/workspace/room/device/device-add-form.component';
import DeviceEditFormComponent from './modules/workspace/room/device/device-edit-form.component';
import DeviceAttributeAddFormComponent from './modules/workspace/room/device/device-attribute/device-attribute-add-form.component';
import DeviceAttributeEditFormComponent from './modules/workspace/room/device/device-attribute/device-attribute-edit-form.component';

// Routing
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';

// Css
import './index.css';

const routing = (
  <Router>
    <div>
        <AppBar color="primary" position="static">
          <Toolbar>
              <TypoGraphy variant="title" color="inherit">
                Smart home
              </TypoGraphy>
              <NavBar>
              </NavBar>
          </Toolbar>
      </AppBar>
      <Switch>
        <Route exact path="/dashboard" component={App} />
        <Route exact path="/workspace" component={ WorkspaceListComponent } />
        <Route exact path="/workspace/add" component={ WorkspaceAddFormComponent } />
        <Route exact path="/workspace/edit/:uid" component={ WorkspaceEditFormComponent } />
        <Route exact path="/workspace/edit/:uid/room/add" component={ RoomAddFormComponent } />
        <Route exact path="/workspace/edit/:uid/room/edit/:roomUid" component={ RoomEditFormComponent } />
        <Route exact path="/workspace/edit/:uid/room/edit/:roomUid/device/add" component={ DeviceAddFormComponent } />
        <Route exact path="/workspace/edit/:uid/room/edit/:roomUid/device/edit/:deviceUid" component={DeviceEditFormComponent} />
        <Route exact path="/workspace/edit/:uid/room/edit/:roomUid/device/edit/:deviceUid/attribute/add" component={DeviceAttributeAddFormComponent} />
        <Route exact path="/workspace/edit/:uid/room/edit/:roomUid/device/edit/:deviceUid/attribute/edit/:deviceAttributeUid" component={DeviceAttributeEditFormComponent} />
      </Switch>
    </div>
  </Router>
)

ReactDOM.render(
  routing,
  document.getElementById('root')
);
